# DZIAŁA XDDD
# data prepz
import pandas as pd
import numpy as np

# training data frame
# get only 10k rows to save time during dev
x_train_df = pd.read_csv("../input/training_set.csv", nrows=7000)
x_train = x_train_df.values
train_dict = {}

for x in x_train:
    data = train_dict.get(x[0])
    if (data is None):
        data = [x[1:]]
    else:
        data.append(x[1:])
    train_dict.update({x[0] : data })
mid_result = train_dict.values()
result = []
for x in mid_result:
    # it looks that most of the data is at leas 255 so this is
    if len(x) >= 255:
        result.append(x[:255])

result = np.array([result])
if result.shape != (1, 21, 255, 5):
    raise Exception
if result.ndim != 4:
    raise Exception
