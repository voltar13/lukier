import os
import pandas as pd
import numpy as np

def normalize(x):
    exp_val = np.sum(x) / len(x)
    norm_x = x - np.array( exp_val )
    var = np.sqrt( np.sum( norm_x**2 ) )
    if (var != 0):
        return norm_x / var
    else:
        return np.zeros( len(x) )

def text_to_normalized_numbers(x):
    names = []
    counter = []
    indexes = np.zeros( len(x) )
    i=0
    for name in x:
        if ( name in names ):
            ind = names.index( name )
            counter[ind] += 1
            indexes[i] = ind
        else:
            names.append( name )
            counter.append( 1 )
            ind = len( names ) - 1
            indexes[i] = ind   
        i += 1
    norm_counter = np.array( normalize( counter ) )
    norm_numb_x = np.array( np.zeros( len(x) ) )
    for i in range (0, len(x)):
        norm_numb_x[i] = norm_counter[ int(indexes[i]) ] 
    return norm_numb_x

def mount_google_drive():
    from google.colab import drive
    drive.mount('/content/drive/')

def get_data_dir():
    if os.path.isdir('../input'):
        return '../input'
    else:
        mount_google_drive()
    if os.path.isdir('/content/drive/My Drive/Colab Notebooks/lukier/whr/input'):
        global test_enabled
        test_enabled = False
        return '/content/drive/My Drive/Colab Notebooks/lukier/whr/input'
    else:
        raise FileNotFoundError
        
data_dir = get_data_dir()
files_names=["2015.csv", "2016.csv", "2017.csv"]
for file_name in files_names:
    data_got = pd.read_csv( data_dir + "/" + file_name )
    data_prep = data_got
    column_names = list( data_got )
    for i in range (0, len( data_got.columns ) ):
        data_type = type( data_got.iloc[0,i] )
        if( data_type != str ):
            data_normalized = normalize ( data_got.iloc[:,i] )
        else:
            data_normalized = text_to_normalized_numbers ( data_got.iloc[:,i] )
        new_values = pd.DataFrame( {column_names[i]: data_normalized} )
        data_prep.update( new_values )    
    data_prep.to_csv( data_dir + "/prepared_input/" + file_name )